# Public Personal Files

These are my personal files that is made available for viewing to the public.

## Contents:
[CV](https://gitlab.com/calfeche/public-personal-files/-/raw/main/files/CV%20-%20Chosen%20Realm%20Alfeche.pdf)<br>
[Resume](https://gitlab.com/calfeche/public-personal-files/-/raw/main/files/Resume%20-%20Chosen%20Realm%20Alfeche.pdf)
